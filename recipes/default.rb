#
# Cookbook:: motd_attributes
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
#
node.default['motd_attributes']['message'] = "It's a beautiful day in the neighborhood..."
node.default['motd_attributes']['message'] = 'Today sucks.'
template '/etc/motd' do
  source 'motd.erb'
  mode '0644'
end
